import * as CSS from 'csstype'

export const statusBusyStyle: CSS.Properties= {
    color: '#f65f00',
}

export const statusFreeStyle: CSS.Properties= {
    color: '#57a279',
}